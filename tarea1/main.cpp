#include<iostream>
#include<cstdio>
#include <fstream>
#include "include/predictor.hpp"
#include "include/bimodal.hpp"
#include "include/hglobal.hpp"
#include "include/privado.hpp"
#include "include/torneo.hpp"
using namespace std;

void funbimod(int s,int bp,int gh,int ph, int o){
    long pc;
    unsigned char taken;
    char predicc;
    bimodal pred=bimodal(s);
    string nombre="output/bimodal.txt";
    while(fscanf(stdin, "%ld %s", &pc, &taken)!=-1){
        predicc=pred.prediccion(pc,taken);
        if(o==1){
            pred.salida(pc,taken,predicc,nombre);
        }
    }
    pred.resumen(s,bp,gh,ph);
}

void funpshare(int s,int bp,int gh,int ph, int o){
    long pc;
    unsigned char taken;
    char predicc;
    privado pred=privado(s,ph);
    string nombre="output/pshare.txt";
    while(fscanf(stdin, "%ld %s", &pc, &taken)!=-1){
        predicc=pred.prediccion(pc,taken);
        if(o==1){
            pred.salida(pc,taken,predicc,nombre);
        }
    }
    pred.resumen(s,bp,gh,ph);
}

void fungshare(int s,int bp,int gh,int ph, int o){
    long pc;
    unsigned char taken;
    char predicc;
    hglobal pred=hglobal(s,gh);
    string nombre="output/gshare.txt";
    while(fscanf(stdin, "%ld %s", &pc, &taken)!=-1){
        predicc=pred.prediccion(pc,taken);
        if(o==1){
            pred.salida(pc,taken,predicc,nombre);
        }
    }
    pred.resumen(s,bp,gh,ph);
}


void funtorneo(int s,int bp,int gh,int ph, int o){
    long pc;
    unsigned char taken;
    char predicc;
    torneo pred=torneo(s,gh,ph);
    string nombre="output/torneo.txt";
    while(fscanf(stdin, "%ld %s", &pc, &taken)!=-1){
        predicc=pred.prediccion(pc,taken);
        if(o==1){
            pred.salida(pc,taken,predicc,nombre);
        }
    }
    pred.resumen(s,bp,gh,ph);
}




int main(int argc, char **argv)
{
    bool correcto=false;
    // parametros
    if(argc==11){
        correcto=true;
    }
    else{
        cout<<"---------------------------------------"<<endl;
        cout<<"    ¡Introdujo mal los argumentos!"<<endl;
        cout<<"---------------------------------------"<<endl;
        cout<<"Recuerde que el formato y orden de parametros es como se muestra:"<<endl<<endl;
        cout<<"Ej: gunzip -c branch-trace-gcc.trace.gz | ./branch -s 3 -bp 2 -gh 4 -ph 4 -o 1"<<endl<<endl;
        cout<<"**El detalle de cada una de las opciones se detalla en la documentación adjunta y en el README.md**"<<endl;
    }
    
    if (correcto){
        int s=stoi(argv[2]);
        int bp=stoi(argv[4]);
        int gh=stoi(argv[6]);
        int ph=stoi(argv[8]);
        int o=stoi(argv[10]);

        switch (bp){
        case 0: 
            funbimod(s,bp,gh,ph,o);
            break;
        case 1: 
            fungshare(s,bp,gh,ph,o);
            break;
        case 2: 
            funpshare(s,bp,gh,ph,o);
            break;
        case 3: 
            funtorneo(s,bp,gh,ph,o);
            break;
        default:
            break;
        }
    }
    
    
    return 0;
}