# Predictores de saltos, Esteban Valverde B47214


## Descripción
El presente programa implementa cuatro tipos de predictores de saltos:
* Bimodal
* Pshare
* Gshare
* Torneo

Para cada predictor se utilizan distintos parámetros, sin embargo, por modularidad para ejecutar la simulación se deberán indicar todos en el orden adecuado, como se describe a continuación, los parámetros que no se requieran simplemente se ignoran.

## Uso
Para ejecutar el programa, se debe hacer uso del siguiente comando:
```bash
$ gunzip -c branch-trace-gcc.trace.gz | ./branch -s 3 -bp 2 -gh 4 -ph 4 -o 1
```
Los parámetros del ejecutable se deben indicar en el orden mostrado, con las banderas indicadas, de lo contrario el programa no funcionará. De utilizarse otro archivo de trazas lo que se debe hacer es cambiar el nombre del archivo mostrado en el comando anterior por el que se desea utilizar.

**NOTA:** Al ejecutar el programa siempre se mostrará en pantalla el resumen de la simulación.

## Parámetros
* **-s:** Cantidad de bits para la tabla BTH, $2^s$ es el tamaño de la tabla.
* **-bp:** Tipo de predictor por utilizar
  * 0: Bimodal
  * 1: Gshare
  * 2: Pshare
  * 3: Torneo
* **-gh:** Número de bits de historia global.
* **-ph:** Número de bits de historia privada.
* **-o:** Decisición de almacenar en un archivo de texto los resultados de las predicciones de los primeros 5000 saltos
  * **0:** No escribir los resultados
  * **1:** Escribir los resultados

El resultado escrito en un archivo de texto se muestra en la carpeta "output" con el nombre del predictor y extensión _.txt_ .

## Notas adicionales
Si se desearan hacer cambios adicionales al código y ejecutarlo bajo el nombre de branch se deberá recompilar, bajo el nombre de _branch_ como se muestra a continuación:

```bash
$ g++ main.cpp src/*.cpp -o branch
```
Con el fin de facilitar el proceso de compilación y pruebas se provee de un makefile que tiene tres comandos:
* **build:** Compila los archivos.
* **run:** Ejecuta el programa con unos parámetros por defecto.
* **clean:** Limpia la carpeta de salida de archivos.

## Documentación
La documentación formal del código se encuentra disponible en el directorio *doc* en el archivo *index.html*. Esta se puede consultar con el fin de entender de una mejor manera el código desarrollado.

## Prueba
Esto es una prueba
