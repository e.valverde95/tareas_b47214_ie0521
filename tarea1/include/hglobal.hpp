#ifndef HGLOBAL_H
#define HGLOBAL_H
#include "predictor.hpp"
/** 
 * La clase hglobal imprementa el predictor con historia global (Gshare)
 *  con su método de predicción de saltos particular.
*/

class hglobal: public predictor{
public:
    /**Mascara para aislar la cantidad de bits requeridos en la historia global*/
    unsigned int gh_mask;
    /**Historia global almacenada en un int largo para tener más posibilidades de
     * almacenamiento de historia.
    */
    unsigned int gh;
    /**
     * Constructor que crea la tabla BHT e inicializa la historia en cero.
    */
    hglobal(int s, int gh);
    /**
     * Constructor por defecto
    */
    hglobal(){};
    /**
     * Destructor, elimina la tabla BHT de memoria dinámica
    */
    ~hglobal();
    /**
     * Método de predicción Gshare
     * @param progctr: Contador de programa
     * @param decision: Output del salto
     * @return predicción: Variable tipo char que indica la predicción bimodal
    */
    char prediccion(long progctr,char decision);
};


#endif