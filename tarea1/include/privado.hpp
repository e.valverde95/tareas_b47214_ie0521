#ifndef PRIVADO_H
#define PRIVADO_H
#include "predictor.hpp"
/**
 * La clase privado imprementa el predictor pshare con su
 *  método de predicción de saltos particular.
*/
class privado: public predictor{
public:
    /**
     * Puntero para almacenar la tabla de historia pht
    */
    unsigned int* bht;
    /**
     * Tamaño de la tabla de historia pht
    */
    int bht_tamano;
    /**
     * Máscara para trabajar exclusivamente con la cantidad
     * de bits de historia deseados
    */
    int bht_mask;

    /**
     * Método de predicción pshare
     * @param progctr: Contador de programa
     * @param decision: Output del salto
     * @return predicción: Variable tipo char que indica la predicción bimodal
    */
    char prediccion(long progctr,char decision);
    /**
     * Constructor de la clase pshare
     * @param s: Parámetro s que indica la cantidad de bits para el tamaño de las tablas bht y pht 2^s
     * @param ph: Parámetro ph que indica la cantidad de bits de historia con los que se trabajará
    */
    privado(int s, int ph);
    privado(){};
    ~privado();
};

#endif