#ifndef BIMODAL_H
#define BIMODAL_H
#include<iostream>
#include<fstream>
#include "predictor.hpp"
using namespace std;
/**
 * La clase bimodal imprementa el predictor bimodal con su método de predicción de saltos particular
*/

class bimodal: public predictor{
public:

    /**
     * El constructor bimodal lo que hace es crear la tabla BHT con todos 
     * los predictores inicializados en cero (SN).
     * @param cantbits: Parámetro s que indica la cantidad de bits para el tamaño de la tabla 2^s
     * */
    bimodal(int cantbits);
    /**
     * Constructor por defecto
    */
    bimodal();
    /**
     * Destructor por defecto, libera de memoria la tabla apuntada por el puntero bht
    */
    ~bimodal();
    /**
     * Función de impresión de estado de predictores
    */
    void imppredic();
    /**
     * Método de predicción bimodal
     * @param progctr: Contador de programa
     * @param decision: Output del salto
     * @return predicción: Variable tipo char que indica la predicción bimodal
    */
    char prediccion(long progctr,char decision);
};

#endif