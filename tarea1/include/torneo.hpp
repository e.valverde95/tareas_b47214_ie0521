#ifndef TOURNAMENT_H
#define TOURNAMENT_H
#include "hglobal.hpp"
#include "privado.hpp"
/**
 * La clase torneo imprementa el predictor por torneo con su
 *  método de predicción de saltos particular.
*/
class torneo: public predictor{
public:
    /**
     * Puntero al predictor interno gshare
    */
    hglobal* gshare;
    /**
     * Puntero al predictor interno pshare
    */
    privado* pshare;
    /**
     * Constructor del predictor
     * @param s: Parámetro s que indica la cantidad de bits para el tamaño de la tabla 2^s
     * @param gh: Parámetro que indica la cantidad de bits de historia por usar en 
     * el predictor con historia global.
     * @param ph: Parámetro que indica la cantidad de bits de historia por usar en el predictor
     * de historia privada.
    */
    torneo(int s, int gh, int ph);
    /**
     * Método de predicción bimodal
     * @param progctr: Contador de programa
     * @param decision: Output del salto
     * @return predicción: Variable tipo char que indica la predicción bimodal
    */
    char prediccion(long progctr,char decision);
    /**
     * Destructor que elimina los objetos de los predictores de memoria dinámica.
    */
    ~torneo();
};

#endif