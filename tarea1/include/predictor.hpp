#ifndef PREDICTOR_H
#define PREDICTOR_H
#include<iostream>
#include<fstream>
using namespace std;
/**
 * La clase predictor es una superclase para hacer uso de la herencia
 * y aprovechar así los elementos en común para los distintos predictores,
 * tanto a nivel de atributos como a nivel de métodos.
*/
class predictor
{
public:
    /**
     * Puntero para almacenar tabla bht, representados en ints
     * 0: SN, 1: WN, 2: WT y 3: ST
    */
    int* t_predictores;
    /**
     * Cantidad de bits para bht
    */
    int numbits;
    /**
     * Tamaño de la bht
    */
    int t_tamano;
    /**
     * Máscara para aislar la cantidad de bits del pc deseada
    */
    int pc_mask;
    /**
     * Cantidad de branches analizados
    */
    long numbranches;
    /**
     * Cantidad de saltos tomados pronosticados como tomados
    */
    long tt;
    /**
     * Cantidad de saltos no tomados pronosticados como tomados
    */
    long ft;
    /**
     * Cantidad de saltos no tomados pronosticados como no tomados
    */
    long tn;
    /**
     * Cantidad de saltos tomados pronosticados como no tomados
    */
    long fn;
    /**
     * Constructor por defecto
    */
    predictor(){};
    /**
     * Destructor por defecto
    */
    ~predictor(){};
    /**
     * Método de impresión genérico de atributos de los predictores
     * @param s: Parámetro s que indica la cantidad de bits para el tamaño de la tabla 2^s
     * @param tp: Valor para identificar el tipo de predictor.
     * @param gh: Cantidad de bits usados para almacenar historia global.
     * @param ph: Cantidad de bits usados para almacenar historia privada.
    */
    void resumen(int s,int tp, int gh, int ph);
    /**
     * Método de escritura de los resultados a un archivo de texto
     * @param progctr: Contador de programa
     * @param result: Output del salto
     * @param pred: Predicción realizada
     * @param dir: Directorio y nombre de archivo para escritura.
    */
    void salida(long progctr,char result, char pred, string dir);
};

#endif