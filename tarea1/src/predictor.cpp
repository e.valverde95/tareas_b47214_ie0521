#include "../include/predictor.hpp"
#include<iostream>
#include<fstream>
using namespace std;

void predictor::resumen(int s,int tp, int gh, int ph){
    string predictor="";
    switch (tp){
    case 0:
        predictor="Bimodal";
        break;
    case 1:
        predictor="GShare";
        break;
    case 2:
        predictor="PShare";
        break;
    case 3:
        predictor="Torneo";
        break;
    default:
        break;
    }
    int size=1<<s;
    
    cout<<"-----------------------------------------------------------------"<<endl;
    cout<<"                       Prediction parameters"<<endl;
    cout<<"-----------------------------------------------------------------"<<endl;
    cout<<"Branch prediction type:\t\t\t\t\t"<<predictor<<endl;
    cout<<"BHT size (entries):\t\t\t\t\t"<<size<<endl;
    cout<<"Global history register size:\t\t\t\t"<<gh<<endl;
    cout<<"Private history register size:\t\t\t\t"<<ph<<endl;
    cout<<"-----------------------------------------------------------------"<<endl;
    cout<<"                        Simulation results"<<endl;
    cout<<"-----------------------------------------------------------------"<<endl;
    cout<<"Number of branches:\t\t\t\t\t"<<this->numbranches<<endl;
    cout<<"Number of correct predictions of taken branches:\t"<<this->tt<<endl;
    cout<<"Number of incorrect predictions of taken branches:\t"<<this->fn<<endl;
    cout<<"Number of correct predictions of not taken branches:\t"<<this->tn<<endl;
    cout<<"Number of incorrect predictions of not taken branches:\t"<<this->ft<<endl;
    double correct=this->tt+this->tn;
    double total=this->numbranches;
    double percent=(correct/total)*100;
    cout<<"Percentage of correct predictions:\t\t\t"<<percent<<" %"<<endl;

}
void predictor::salida(long progctr,char result, char pred, string dir){
    ofstream outfile;
    string resultado=" ";
    if (result==pred){
        resultado="Correcto";
    }
    else{
        resultado="Incorrecto";
    }
    
    if (this->numbranches<=5000){
        if(this->numbranches==1){
            outfile.open (dir);
            outfile<< "PC\t\tOutcome\t\tPrediction\tCorrect/Incorrect\n"<<progctr<<"\t"<<result<<"\t\t"<<pred<<"\t\t"<<resultado<<"\n";
        }else{
            outfile.open (dir,fstream::app);
            outfile<<progctr<<"\t"<<result<<"\t\t"<<pred<<"\t\t"<<resultado<<"\n";
        }
        outfile.close();
    }
}

