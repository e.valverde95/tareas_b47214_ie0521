#include "../include/hglobal.hpp"
#include<iostream>
using namespace std;


hglobal::hglobal(int s, int gh)
{
    // Determinacion de tamano
    int tamano=1<<s;
    // Inicio de variables en cero
    this->fn=0;
    this->ft=0;
    this->tt=0;
    this->tn=0;
    this->numbranches=0;

    this->numbits=s;
    this->t_tamano=tamano;
    
    // Creación de mascara para pc
    this->pc_mask=tamano-1;
    // Inicio de historia en cero
    this->gh=0;
    // Creación de máscara para gh
    this->gh_mask=(1<<gh)-1;
    
    // Asignación de espacio en memoria
    this->t_predictores=new int[tamano];
    // Inicialización de predicctores en cero
    for (int i = 0; i < tamano; i++)
    {
        this->t_predictores[i]=0;
    }
}

hglobal::~hglobal()
{
    delete this->t_predictores;
}

char hglobal::prediccion(long progctr,char decision){
    // Extraccion de bits de interes de pc
    long index_pc=progctr&(this->pc_mask);
    // Extraccion de bits de interes de historia
    long xor_data=(this->gh)&(this->gh_mask);
    // Calculo de indice
    long index=(xor_data^index_pc);
    // Extraccion de bits de interes de indice
    index=index&(this->pc_mask);
    char predicc=' ';
    (this->numbranches)++;
    // actualizar datos y predictores
    if (this->t_predictores[index]<2)
    {
        predicc='N';
    }
    else
    {
        predicc='T';
    }
    
    if (decision=='N'){
        // Actualizar historia con un 0
        (this->gh)=(this->gh)<<1;
        if (predicc=='N'){
            this->tn++;
            if(this->t_predictores[index]==1)this->t_predictores[index]--;
        }
        else{
            this->ft++;
            this->t_predictores[index]--;
        }
    }
    else{
        //actualizar historia con un 1
        (this->gh)=(this->gh)<<1;
        (this->gh)++;
        if (predicc=='T'){
            this->tt++;
            if(this->t_predictores[index]==2)this->t_predictores[index]++;
        }
        else{
            this->fn++;
            this->t_predictores[index]++;
        }
    }   
    return predicc;
}



