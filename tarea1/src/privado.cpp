#include "../include/privado.hpp"
#include<iostream>
using namespace std;


privado::privado(int s, int ph){
    // Determinacion de tamano de las tablas
    int tamano=1<<s;
    // Inicio de variables en cero
    this->fn=0;
    this->ft=0;
    this->tt=0;
    this->tn=0;
    this->numbranches=0;

    this->numbits=s;
    this->t_tamano=tamano;
    this->bht_tamano=tamano;

    // Mascaras para pc y bht
    this->pc_mask=tamano-1;
    this->bht_mask=(1<<ph)-1;
    
    // Tablas de historia privada y predictores
    this->bht=new unsigned int[tamano];
    this->t_predictores=new int[tamano];
    // Inicio de predictores en SN
    for (int i = 0; i < tamano; i++)
    {
        this->t_predictores[i]=0;
    }
    // Inicio de historia en 0
    for (int i = 0; i < tamano; i++)
    {
        this->bht[i]=0;
    }
}

privado::~privado()
{
    delete this->t_predictores;
    delete this->bht;
}

char privado::prediccion(long progctr,char decision){
    // Extracción de bits de interes de pc
    long index_bht=progctr&(this->pc_mask);
    // Extracción de bits de interes de historia
    long xor_data=(this->bht[index_bht])&(this->bht_mask);
    // Calculo de indice
    long index=(xor_data^index_bht);
    // Extraccion de bits de interes del indice
    index=index&(this->pc_mask);
    
    char predicc=' ';
    (this->numbranches)++;
    // actualizar datos y predictores
    if (this->t_predictores[index]<2)
    {
        predicc='N';
    }
    else
    {
        predicc='T';
    }
    if (decision=='N'){
        // Actualizar historia con un 0
        (this->bht[index_bht])=(this->bht[index_bht])<<1;
        if (predicc=='N'){
            this->tn++;
            if(this->t_predictores[index]==1)this->t_predictores[index]--;
        }
        else{
            this->ft++;
            this->t_predictores[index]--;
        }
    }
    else{
        //actualizar historia con un 1
        (this->bht[index_bht])=(this->bht[index_bht])<<1;
        (this->bht[index_bht])++;
        if (predicc=='T'){
            this->tt++;
            if(this->t_predictores[index]==2)this->t_predictores[index]++;
        }
        else{
            this->fn++;
            this->t_predictores[index]++;
        }
    }   
    return predicc;
}
