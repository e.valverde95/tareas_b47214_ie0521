#include "../include/torneo.hpp"
#include<iostream>
using namespace std;


torneo::torneo(int s, int gh, int ph){
    // Asignacion de memoria y creacion de predictores internos
    this->gshare=new hglobal(s,gh);
    this->pshare=new privado(s,ph);
    // Calculo del tamaño de bht
    int tamano=1<<s;
    // Inicio de variables en 0
    this->fn=0;
    this->ft=0;
    this->tt=0;
    this->tn=0;
    this->numbranches=0;

    this->numbits=s;
    this->t_tamano=tamano;
    
    // Calculo de mascara para pc
    this->pc_mask=tamano-1;
    // Asignacion de memoria de bht
    this->t_predictores=new int[tamano];
    // Inicio de predictores en Strongly Prefered Pshare
    for (int i = 0; i < tamano; i++)
    {
        this->t_predictores[i]=0;
    }
}

torneo::~torneo()
{
    // Liberacion de memoria
    delete this->t_predictores;
    delete this->pshare;
    delete this->gshare;
}

char torneo::prediccion(long progctr,char decision){
    (this->numbranches)++;
    // Calculo de indice por pc y mascara
    long index=progctr&(this->pc_mask);
    char predg=this->gshare->prediccion(progctr,decision);
    char predp=this->pshare->prediccion(progctr,decision);
    char predicc=' ';
    
    // Generacion de prediccion
    if (this->t_predictores[index]<2){
        predicc=predp;
    }
    else{
        predicc=predg;
    }

    // actualizar metapredictor
    if (predg!=predp){
        if (predg==decision){
            if(this->t_predictores[index]<3){
                (this->t_predictores[index])++;
            }
        }
        else{
            if(this->t_predictores[index]>0){
                (this->t_predictores[index])--;
            }
        }
    }
    // actualizar datos
    if (decision=='N'){
        if (predicc=='N'){
            this->tn++;
        }
        else{
            this->ft++;
        }
    }
    else{
        if (predicc=='T'){
            this->tt++;
        }
        else{
            this->fn++;
        }
    } 
    return predicc;
}