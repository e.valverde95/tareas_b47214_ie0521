#include "../include/bimodal.hpp"
#include<iostream>
using namespace std;

bimodal::bimodal(){

}
bimodal::bimodal(int cantbits){
    // Determinación de tamaño
    int tamano=1<<cantbits;
    // Inicio en cero de parámetros
    this->fn=0;
    this->ft=0;
    this->tt=0;
    this->tn=0;
    this->numbranches=0;
    this->numbits=cantbits;
    this->t_tamano=tamano;
    // Almacenamiento de máscara
    this->pc_mask=tamano-1;
    // Asignación de arreglo en memoria
    this->t_predictores=new int[tamano];

    // Inicio de predictores en SN
    for (int i = 0; i < tamano; i++)
    {
        this->t_predictores[i]=0;
    }
}

bimodal::~bimodal(){
    delete this->t_predictores;
}

void bimodal::imppredic(){
    for (int i = 0; i < this->t_tamano; i++)
    {
        cout<<this->t_predictores[i]<<", ";
    }
    cout<<endl;
}

char bimodal::prediccion(long progctr,char decision){
    // Extraccion de bits de interes del pc
    long index=progctr&(this->pc_mask);
    char predicc=' ';
    (this->numbranches)++;
    if (this->t_predictores[index]<2)
    {
        predicc='N';
    }
    else
    {
        predicc='T';
    }
    
    // actualizar datos y predictores
    if (decision=='N'){
        if (predicc=='N'){
            this->tn++;
            if(this->t_predictores[index]==1)this->t_predictores[index]--;
        }
        else{
            this->ft++;
            this->t_predictores[index]--;
        }
    }
    else
    {
        if (predicc=='T'){
            this->tt++;
            if(this->t_predictores[index]==2)this->t_predictores[index]++;
        }
        else{
            this->fn++;
            this->t_predictores[index]++;
        }
    }   
    return predicc;
}
